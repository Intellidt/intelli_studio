<!-- Footer -->
<div class="container-fluid position-relative">
    <footer class="site-footer">
        <div class="container">
            <div class="row py-4 d-block text-center">
                <h5>© Copyright 2019 Intelli Management Group. All Rights Reserved. &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                Designed by&nbsp;<a href="http://intellimanagement.com" target="_blank" style="color:purple">Intelli Management Group</a></h5>
            </div>
        </div>
    </footer>
</div>
</div>
</body>
</html>