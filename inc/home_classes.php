<div class="our_classes">
    <div class="container">
        <div class="row">
            <div class="text-center w-100 mt-5">
                <h3 class="title"><?php echo $lang["our_programs_n_classes"] ?></h3>
                <div class="leaf-border"><img src="images/home/home_leaf.png" alt="Border"/></div>
            </div>
        </div>
        <div class="row m-0">
            <div class="owl-carousel mt-4">
                <div class="card">
                    <a href="class.php">
                        <div class="d-inline-block w-100">
                            <img src="images/classes/intelli_kids_class_preschool.jpg" class="card-img-top" alt="<?php echo $lang["preschool_learning_program"]["title"] ?>">
                        </div>
                        <div class="card-body pt-5 position-relative">
                            <h6 class="card-title text-uppercase color-Prussianblue text-center"><?php echo $lang["preschool_learning_program"]["title"] ?></h6>
                            <p class="card-text color-gray40"><?php echo $lang["preschool_learning_program"]["description"] ?></p>
                        </div>
                    </a>
                </div>
                
                
                <div class="card">
                    <a href="class.php">

                    <div class="d-inline-block w-100">
                        <img src="images/classes/intelli_kids_class_study.jpg" class="card-img-top" alt="<?php echo $lang["study_class"]["title"] ?>">
                    </div>
                    <div class="card-body pt-5 position-relative">
                        <h6 class="card-title text-uppercase color-Prussianblue text-center"><?php echo $lang["study_class"]["title"] ?></h6>
                        <p class="card-text color-gray40 font-weight-light"><?php echo $lang["study_class"]["description"] ?></p>
                    </div>
                    </a>
                </div>
                
                
                <div class="card">
                    <a href="class.php">

                    <div class="d-inline-block w-100">
                        <img src="images/classes/intelli_kids_class_math.jpg" class="card-img-top"
                             alt="<?php echo $lang["singapore_math"]["title"] ?>">
                    </div>
                    <div class="card-body pt-5 position-relative">
                        <h6 class="card-title text-uppercase color-Prussianblue text-center"><?php echo $lang["singapore_math"]["title"] ?></h6>
                        <p class="card-text color-gray40 font-weight-light"><?php echo $lang["singapore_math"]["description"] ?></p>
                    </div>
                    </a>
                </div>
                
                
                
                 <div class="card">
                     <a href="class.php">

                     <div class="d-inline-block w-100">
                        <img src="images/classes/intelli_kids_class_100.jpg" class="card-img-top" alt="<?php echo $lang["100_series"]["title"] ?>">
                    </div>
                    <div class="card-body pt-5 position-relative">
                        <h6 class="card-title text-uppercase color-Prussianblue text-center"><?php echo $lang["100_series"]["title"] ?></h6>
                        <p class="card-text color-gray40 font-weight-light"><?php echo $lang["100_series"]["description"] ?></p>
                    </div>
                     </a>
                </div>
                
                
                <div class="card">
                    <a href="class.php">

                    <div class="d-inline-block w-100">
                        <img src="images/classes/intelli_kids_class_oxford.jpg" class="card-img-top" alt="<?php echo $lang["oxford_reading_tree"]["title"] ?>">
                    </div>
                    <div class="card-body pt-5 position-relative">
                        <h6 class="card-title text-uppercase color-Prussianblue text-center"><?php echo $lang["oxford_reading_tree"]["title"] ?></h6>
                        <p class="card-text color-gray40"><?php echo $lang["oxford_reading_tree"]["description"] ?></p>
                    </div>
                    </a>
                </div>
 
                
                <div class="card">
                    <a href="class.php">

                    <div class="d-inline-block w-100">
                        <img src="images/classes/intelli_kids_class_jump.jpg" class="card-img-top" alt="<?php echo $lang["jump_series"]["title"] ?>">
                    </div>
                    <div class="card-body pt-5 position-relative">
                        <h6 class="card-title text-uppercase color-Prussianblue text-center"><?php echo $lang["jump_series"]["title"] ?></h6>
                        <p class="card-text color-gray40"><?php echo $lang["jump_series"]["description"] ?></p>
                    </div>
                    </a>
                </div>
                
                
                <div class="card">
                    <a href="class.php">

                    <div class="d-inline-block w-100">
                        <img src="images/classes/intelli_kids_class_stem.jpg" class="card-img-top" alt="<?php echo $lang["stem"]["title"] ?>">
                    </div>
                    <div class="card-body pt-5 position-relative">
                        <h6 class="card-title text-uppercase color-Prussianblue text-center"><?php echo $lang["stem"]["title"] ?></h6>
                        <p class="card-text color-gray40"><?php echo $lang["stem"]["description"] ?></p>
                    </div>
                    </a>
                </div>
                
                
                <div class="card">
                    <a href="class.php">

                    <div class="d-inline-block w-100">
                        <img src="images/classes/intelli_kids_class_arts.jpg" class="card-img-top"
                             alt="<?php echo $lang["creative_kids"]["title"] ?>">
                    </div>
                    <div class="card-body pt-5 position-relative">
                        <h6 class="card-title text-uppercase color-Prussianblue text-center"><?php echo $lang["creative_kids"]["title"] ?></h6>
                        <p class="card-text color-gray40 font-weight-light"><?php echo $lang["creative_kids"]["description"] ?></p>
                    </div>
                    </a>
                </div>
                
                
                 <div class="card">
                     <a href="class.php">

                     <div class="d-inline-block w-100">
                        <img src="images/classes/intelli_kids_class_lego.jpg" class="card-img-top" alt="<?php echo $lang["lego_program"]["title"] ?>">
                    </div>
                    <div class="card-body pt-5 position-relative">
                        <h6 class="card-title text-uppercase color-Prussianblue text-center"><?php echo $lang["lego_program"]["title"] ?></h6>
                        <p class="card-text color-gray40 font-weight-light"><?php echo $lang["lego_program"]["description"] ?></p>
                    </div>
                     </a>
                </div>
                
                <div class="card">
                    <a href="class.php">

                    <div class="d-inline-block w-100">
                        <img src="images/classes/intelli_kids_class_chinese.jpg" class="card-img-top" alt="<?php echo $lang["chinese_classes"]["title"] ?>">
                    </div>
                    <div class="card-body pt-5 position-relative">
                        <h6 class="card-title text-uppercase color-Prussianblue text-center"><?php echo $lang["chinese_classes"]["title"] ?></h6>
                        <p class="card-text color-gray40 font-weight-light"><?php echo $lang["chinese_classes"]["description"] ?></p>
                    </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
