<?php
session_start();
header('Cache-control: private'); // IE 6 FIX

if(isset($_GET['lang']) && !empty($_GET['lang']))
{
    // register the session and set the cookie
    $_SESSION['lang'] = $_GET['lang'];

    if(isset($_SESSION['lang']) && $_SESSION['lang'] != $_GET['lang']){
        echo "<script type='text/javascript'> location.reload(); </script>";
    }
}
// Include Language file
if(isset($_SESSION['lang'])){
    include "lang/".$_SESSION['lang']."/intelli_studio_content_".$_SESSION['lang'].".php";
}else{
    include "lang/cn/intelli_studio_content_cn.php";
}
?>
