<?php

$lang = array();
$lang["website_title"] = "Intelli Training Center";
$lang["metadescription"]="Intelli Training Center offers the most comprehensive teaching courses, in which students will receive proper guidance needed to excel at home and at school.";


/**********************************/
/*            HOME PAGE           */
/**********************************/


$lang["home"] = "Home";
$lang["aboutus"] = "About us";
$lang["classes"] = "Classes";
$lang["contactus"] = "Contact us";
$lang["slider1_heading"]="We believe success is built on a sturdy foundation";
$lang["slider1_detail"]="Students who study at Intelli Training Center have a 90% success rate in enrolling into prestigious Private Schools and Universities.";
$lang["slider2_heading"]="Help your children grow";
$lang["slider2_detail"]="We are a forward-looking, dynamic company offering progressive learning programs to guide and lead children to a prosperous future filled with opportunities.";
$lang["slider3_heading"]="Innovative programs to enhance learning";
$lang["slider3_detail"]="We provide creative, integrated, and effective training courses to assist children in becoming self-sufficient and confident adults.";

$lang["discover_more"]="Discover more";
$lang["read_more"]="Read more";
$lang["load_more"]="Load more";

$lang["highlight1_heading"]="ACTIVE LEARNING";
$lang["highlight1_description"]="We offer a range of special classes that cater to individual interests as well as after-school programs for children of all ages.";
$lang["highlight2_heading"]="EXPERIENCE TEACHERS";
$lang["highlight2_description"]="Our teachers have over 20 years of experience in guiding children to learn and comprehend knowledge more effectively.";
$lang["highlight3_heading"]="INNOVATIVE PROGRAMS";
$lang["highlight3_description"]="We incorporate the use of multimedia, storytelling, games and crafts in our programs to facilitate the love of learning in our students.";
$lang["highlight4_heading"]="PROGRESSIVE PROGRAMS AVAILABLE";
$lang["highlight4_description"]="Our programs are structured to lead and teach children to learn step-by-step, focusing on areas which need more guidance.";

$lang["home_about_heading"]="About Intelli Training Center";
$lang["home_about_detail"]="Our Training Center offers the most comprehensive teaching courses, with an array of various different subjects and classes. Our programs ensure that children in our care will get the proper guidance they need to excel both at home and at school.";
$lang["home_about_points"][0]="Positive learning environment for your child";
$lang["home_about_points"][1]="Diverse selection of hobby class and extracurricular programs";
$lang["home_about_points"][2]="Develop personalized training courses based on individual abilities";


$lang["our_programs_n_classes"] = "Our Programs & classes";

$lang["counter1"]="ENROLLMENT SUCCESS RATE TO PRESTIGIOUS SCHOOLS";
$lang["counter2"]="DIFFERENT CLASSES AND PROGRAMS TO CHOOSE FROM";
$lang["counter3"]="BENEFITED FROM OUR CHILDREN’S PROGRAMS AND CLASSES";
$lang["counter4"]="YEARS OF TEACHING EXPERIENCE";

$lang["special_program_heading"]="Special Programs";
$lang["special_program1"]="STEM";
$lang["special_program1_detail"]="Our Science, Technology, Engineering, and Mathematic program allows children to learn this knowledge beyond the classroom through structured play.";
$lang["special_program2"]="Creative Kids";
$lang["special_program2_detail"]="Grab your crayons, glue, glitter, and paint, and let your creativity shine! Fun and creative art & craft program that kids of all ages will love!";
$lang["special_program3"]="Lego Fun";
$lang["special_program3_detail"]="Children will have fun building models through our Lego program, while also learning about structures and physics.";
$lang["special_program4"]="Kids Computer";
$lang["special_program4_detail"]="Introduce children to computer technology, cultivating their development in problems solving skills and preparing them for their limitless future.";


$lang["whychoose_heading"]="Why Choose Intelli Training Center";
$lang["whychoose1"]="Experienced Teachers";
$lang["whychoose1_detail"]="Our teachers care about the progress of your children and will work diligently with you in guiding your child to greater success";
$lang["whychoose2"]="Progressive Learning Module";
$lang["whychoose2_detail"]="Children will be guided through a step-by-step learning module in building a solid foundation for faster learning of future new materials";
$lang["whychoose3"]="Array of Programs Available";
$lang["whychoose3_detail"]="Over 20 different programs and classes available in training your child for a brighter future";
$lang["whychoose4"]="Proven Track Record";
$lang["whychoose4_detail"]="Students who study at Intelli Training Center have a 90% success rate enrolling in prestigious Private Schools and Universities";
$lang["home_contactus"]="Contact Us Now";



/**********************************/
/*            About US            */
/**********************************/

$lang["aboutus1_title"] = "Intelli Training Center";
$lang["aboutus1_detail"] = "Intelli Training Center was founded in Vancouver, Canada, in 2002. With a rise in immigration and foreign students from abroad, our founders understood the difficulties associated with integrating into a new environment. Our goal was to guide and assist children from multicultural family to not only successfully assimilate into the Canadian Education System but also to be happy, confident, and to develop a desire for knowledge.</p>
<p>Intelli Training Center focus in developing a progressive training program based on the individual student's level in order to achieve different learning objectives, which enable each of our students to nurture and expand their unique interests and achievements.";

$lang["aboutus2_title"] = "Frequent Assessment";
$lang["aboutus2_detail"] = "We strive to assist every child with learning difficulties and will provide immediate advice or assessment to enhance students' interest in learning and strengthen their foundation. At the same time, we will also maintain constant communication with the parents with updates of the students' progress.";

$lang["aboutus_need_help"] = "Need Help? Call Us Now";



/**********************************/
/*            Classes             */
/**********************************/


$lang["classes_n_programs"] = "Classes &amp; Programs";

$lang["classes_category"] = array("all" => "All", "study_classes" => "Study Classes", "elite_classes" => "Elite Classes", "special_programs" => "Special Programs", "age_4_5" => "Age 4-5", "age_6_7" => "Age 6-7", "age_8_13" => "Age 8-13");




$lang["preschool_learning_program"] = array("title" => "Preschool Learning Program", "description" => "Suitable for children ages 3 to 5, this program enabled students to learn and explore in a positive and nurturing environment. Through storytelling, multimedia, games and crafts which inspires children to develop the love of learning, while encouraging them to express themselves and foster self-esteem in group activities. Our course promotes student’s reading and writing ability, improving their vocabulary and conversational English, as well as basic Mathematic skills. Children achieve learning milestones that lay the foundation for paving the path to start school.");

$lang["study_class"] = array("title" => "Study Class", "description" => "Suitable for school aged children currently studying in grade K–12. This program aims to excel the comprehension on curriculum materials and guidance in completing homework assignments and projects. We will closely monitor each student's progress and develop a personalized study plan for individual academic needs. Through successful coaching strategies, we will improve the areas of study needing special attention and enhancement, encourage students to achieve their learning goals, and motivate them to bring their passion for learning back to their daily curriculum.");

$lang["intelli_social_network_club"] = array("title" => "Intelli Social Network Club", "description" => "A child’s good social and emotional skills might be the biggest predictor of success in adulthood. Intelli Social Network Club is the best place to broaden students’ horizons and expand their social skills through board games or other group activities. Children will learn how to cooperate, contribute, participate, follow instructions, set common goals, help out and at the same time pick up skills like negotiation, conflict resolution, communication, emotion control, assertiveness, bargaining, public-speaking, etc. These skills teach children to face future challenges fearlessly and be able to tackle complex situations.");




$lang["singapore_math"] = array("title" => "Singapore Math", "description" => "Singapore Math method is a highly effective teaching approach, consistently ranking at the top in international math testing and adapted to more and more schools in North America. These days, students require more than just rote learning to excel in Maths. By applying Singapore Math we’re able to teach our student to focus on skills such as critical thinking, heuristics, logical reasoning and modelling techniques and more. Students attending will be well equipped with an array of skills to confidently embrace future challenges in the world of numbers.");

$lang["100_series"] = array("title" => "100 Series", "description" => "The 100 Series will surely keep your kids on their toes and minds sharp with lots fun and engaging exercises. Separated into 5 categories, Math, English, Writing, Spelling, and Reading, students will be able to stay focus and guided to work on particular areas they may have difficulties with. Our 100 Series is an all-inclusive course addressed and improve upon problem solving, critical thinking, as well as fundamental skills necessary in school. Our program is the perfect solution for children to polish their foundations.");

$lang["oxford_reading_tree"] = array("title" => "Oxford Reading Tree", "description" => "With UK's number one reading scheme, over millions of children have learnt not only to read but to love to read. We offer the Oxford Reading Tree program suitable for children ages from 4-12.  Children will learn essential skills like reading, phonics, comprehension, blending of sounds, vocabularies and so much more. Each book is beautifully illustrated with storyline based on daily life to keep readers engaged. We inspire students' reading potential, stimulating them to practice and perfect essential language skills, allowing them to excel at school as they read and establish lifetime reading power!");

$lang["jump_series"] = array("title" => "Jump Series", "description" => "The Jump Series offer Jump Math and Jump English. The concept of using the word “Jump” means to skip a grade. Students are expected to discover and understand concepts through answering questions and working through challenges on their own. In addition, positive guidance and feedback will be supported by the teacher to help students to have a deeper understanding of these concepts. Students who attend these classes tend to progress into harder materials quickly and “jump” ahead in class.");

$lang["summer_get_set_go"] = array("title" => "Summer Get Set Go!", "description" => "Our summer program is designed to help children to get ready for the next grade year. Incorporating both English and Math, our Summer course is filled to the brim with activities and fun. Students will be able to keep their minds sharp throughout the summer break. Through different activities they will be actively working on improving their reading comprehension, conversational proficiency, broadening their vocabulary and stepping their way to success as they have fun. At the end of the course, your child will be well equipped to take on any challenges of the new school year.");

$lang["chinese_classes"] = array("title" => "Chinese Classes", "description" => "Chinese is one of the most widely-spoken language in the world. Each character reflects the beauty and culture of ancient China. Learning Chinese as a second language will open the doors to a brighter future for young learners. Our classes are suitable for children ages 4 and older, and will emphasize on Chinese reading and writing skills, listening and conversation, as well as moral education. Children will learn Chinese grammar, proverbs, stories, history, and traditional festivals. The class is tailored for students whose first language is English, and are aware of the cultural value and significance of the Chinese language of our heritage.");

$lang["reading_genius"] = array("title" => "Reading Genius", "description" => "Developing how to read is an important skill for children to learn early on, but becoming a reading genius needs more training. At Intelli Training Center, we make the reading journey engaging and immersive by providing content that pique the learner’s curiosity. Classes are suitable for children ages 6 and older. Students will be improving on their comprehension and understanding while becoming more fluent at reading and writing. Interactive reading materials are specially selected and customized to help guide and keep each and every individual student engaged.");

$lang["computer_explorer"] = array("title" => "Computer Explorer", "description" => "Learning the fundamentals about computers, greatly helps prepare children in using the numerous forms of technology presented in today’s society. As the digital space is growing ever so vast, children should be equipped with the necessary knowledge and tools to explore this new world. Our young explorers will be learning the different uses of computer, including word processing, basic computer concepts, website navigation and how to obtain appropriate and safe resources online and more. In a world where computer literacy is going from a competitive advantage to a necessity, it's never too early to introduce computers into a child's education.");

$lang["canadian_101"] = array("title" => "Canadian 101", "description" => "Your children can learn and explore the difference between Canada and your home country. Suitable for children ages 9 and above, through reading related articles, this course will introduce children to important features of this country, such as the Canadian climate, natural resources, geographical analysis, lifestyle, Canadian culture and current Canadian events, etc. This helps to expand students’ horizon of Canada, while enhancing their ability to read and understand informational articles.");

$lang["wow_in_the_world"] = array("title" => "Wow in the World", "description" => "Suitable for children age 11 or older, this course is designed to encourage students stay close to the community and the world around them. Kids need real-time, relevant and nonfiction reading experience which promotes critical thinking and inferential skills.  Children will be enthralled by reading articles about the international, Canadian or community, even articles on science, sports, fun or current news. Lets’ your kids dive into a journey of wonders of the world!");

$lang["typing_guru"] = array("title" => "Typing Guru", "description" => "Living in today’s digital driven world, typing has become an important skill for children to learn. Through a series of professionally-designed interactive and easy-to-follow typing exercise, students will learn correct keyboard skills from how to properly position their hands to build up their muscle memory. Students can master their speed, accuracy, and computer confidence while increasing their vocabularies as well as improving spelling and writing structure.");




$lang["creative_kids"] = array("title" => "Creative Kids", "description" => "Grab your crayons, paint, glue and glitter, and let your creativity shine! Fun and creative arts & craft program that kids of all ages will love! Children can explore different materials, colors or patterns to create works of art that express their creativity and individuality. Practicing arts and crafts can make children more enthusiastic about other aspects of lives, it also benefit kids emotionally and have positive effect for improving learning abilty, social skills or promote self-esteem.");

$lang["stem"] = array("title" => "STEM", "description" => "In our 21st century, scientific and technological innovations have become increasingly important, which changes the way students learn, connect and interact every day. STEM integrates Science, Technology, Engineering, and Mathematic knowledge into a cohesive learning paradigm based on real-world applications. Our project-based learning experiences introduce children to core STEM concept and the connection for how it is applied in the real world. Skills developed by students help them to translate their future dreams into reality and to succeed at school and beyond.");

$lang["lego_program"] = array("title" => "Lego&reg; Fun", "description" => "LEGO program is designed to ignite students’ natural curiosity, helping them develop essential communication, creativity, team spirit, and logical thinking skills in a fun and exciting way. The LEGO Mechanical Series provides students an in-depth understanding of the relationship between structure, mathematics and physics in exploring core STEM concepts. By building, designing, and testing solutions, students work as young scientists and engineers while honing how the real world works.");

$lang["junior_chef"] = array("title" => "Junior Chef", "description" => "An edible education that is joyful, fun and delicious for some of the younger chefs in the family. Children can learn about texture, taste and flavour by enjoying the fun of cooking while practicing some basic math concepts and building vocabulary. Our junior chef can learn how to hone their fine-motor skills, stirring, whisking, kneading, sprinkling and even cutting with a plastic knife. Anything from playing with doughs or other ingredients to making delicious pastries and snacks for everyone to enjoy. ");

$lang["summer_speak_up_class"] = array("title" => "Summer Speak-Up Class", "description" => "Quality public speaking skills are an intangible asset that might help your child to become great leaders one day. Our speak-up class teach children to write their own speeches, engage with the audience, awareness of vocal projection, as well as building self-esteem and develop charisma. Our goal is to develop an outgoing, well-organized and motivated children. You will be surprised how your children shine on the stage and discover their priceless potential that will benefit for the rest of their life.");

$lang["summer_field_trip"] = array("title" => "Summer Field Trip", "description" => "Summer field trips are not only lots of fun but they can also an invaluable learning experiences for young children. Our top notch program offer children the opportunities to explore and venture outside of the classroom and having fun interacting with new surroundings. Whether the trip is visiting a farm, a museum or working as a junior barista at a bubble tea store, each experience enable students to create a connection between school and the ‘real-world’. Students who go on field trips become more understanding and respectful towards different culture and the society as a whole.");




/**********************************/
/*            Contact US          */
/**********************************/


$lang["contactus_title"] = "Get in Touch";
$lang["contactus_intro"] = "Please follow us on Facebook for the latest news and updates, and contact us for more program and class information.";
$lang["contactus_follow"] = "Follow us";
?>
