<?php

$lang = array();
$lang["website_title"] = "Intelli Training Center 才藝訓練中心";
$lang["metadescription"]="Intelli Training Center才藝訓練中心提供最全面的教學課程，導師會提供學生適當的指導，令孩子在家中和學校皆取得卓越表現。";


/**********************************/
/*            HOME PAGE           */
/**********************************/


$lang["home"] = "主頁";
$lang["aboutus"] = "關於我們";
$lang["classes"] = "各項課程";
$lang["contactus"] = "聯絡我們";
$lang["slider1_heading"]="成功是建立在穩固的基礎上的";
$lang["slider1_detail"]="在Intelli Training Center學習的學生有90％入讀著名的私立學校和大學的成功率。";
$lang["slider2_heading"]="幫助孩子成長";
$lang["slider2_detail"]="我們是一家充滿創意、前瞻性及充滿活力的培訓中心，提供漸進式學習計劃，引導孩子迎接挑戰充滿機遇的未來。";
$lang["slider3_heading"]="提升學習能力";
$lang["slider3_detail"]="我們提富有創造力、綜合性和奏效的課程，幫助孩子增強自信心並快樂地成長。";

$lang["discover_more"]="更多";
$lang["read_more"]="詳情";
$lang["load_more"]="詳情 ";

$lang["highlight1_heading"]="互動式教學";
$lang["highlight1_description"]="我們為不同年齡階段的孩子提供興趣班和課後功課輔導班。";
$lang["highlight2_heading"]="經驗豐富&nbsp;&nbsp;師資優良";
$lang["highlight2_description"]="我們的教師有超過20年教學經驗，指導孩子更有效地學習及領悟知識。";
$lang["highlight3_heading"]="創新課程";
$lang["highlight3_description"]="我們的課程融入多媒體、故事、遊戲和手工藝，致力促進和培養學生對學習的興趣。";
$lang["highlight4_heading"]="漸進式課程";
$lang["highlight4_description"]="我們的課程引導孩子循步漸進地學習，並專注於需要提升的領域。";

$lang["home_about_heading"]="About Intelli 才藝訓練中心";
$lang["home_about_detail"]="我們的培訓中心提供最完善的教學課程，包含各種不同的科目和知識。 我們的課程確保孩子能們獲得正確的指導，讓他們能在學校和生活上都有出色的表現。";
$lang["home_about_points"][0]="正面及積極的學習環境";
$lang["home_about_points"][1]="多樣化的興趣班及課外活動課程可供選擇";
$lang["home_about_points"][2]="根據個別學生的學習水平制定重點培訓班課程";


$lang["our_programs_n_classes"] = "我們的課程";

$lang["counter1"]="名校入學率";
$lang["counter2"]="種不同的課程可供選擇";
$lang["counter3"]="受益於我們制提供的課程";
$lang["counter4"]="年教學經驗";

$lang["special_program_heading"]="特別課程";
$lang["special_program1"]="STEM";
$lang["special_program1_detail"]="我們的STEM課程融合科學、科技、機械和數學的元素，通過有系統的學習過程，教育孩子們在遊戲中領會課堂以外的知識。";
$lang["special_program2"]="創意塗鴉";
$lang["special_program2_detail"]="拿起蠟筆，膠水，閃光和顏色筆，讓孩子的創造力閃耀吧！ 有趣及充滿創意的工作室，所有孩子都會喜歡！";
$lang["special_program3"]="樂高樂";
$lang["special_program3_detail"]="孩子們可運用樂高積木發揮無限創意制作出有趣的模型，同時從制作過程中領誤到結構與物理學等的知識。";
$lang["special_program4"]="兒童電腦班";
$lang["special_program4_detail"]="我們提供優質的培訓課程讓孩子學習電腦知識，並培養他們解決問題的技能，為無限的未來發展鋪路。";


$lang["whychoose_heading"]="為何選擇Intelli 才藝訓練中心？";
$lang["whychoose1"]="經驗豐富師資優良";
$lang["whychoose1_detail"]="我們的老師關心每個孩子的成長，並將與您一起努力指導您的孩子取得更大的成就和進步";
$lang["whychoose2"]="漸進式的學習模式";
$lang["whychoose2_detail"]="引導孩子以循步漸進的學習方式建立鞏固的根基，為日後學習更多新的資料奠定基礎";
$lang["whychoose3"]="一系列課程可供選擇";
$lang["whychoose3_detail"]="超過20種不同的課程，致力培訓您的孩子擁有更光輝的未來";
$lang["whychoose4"]="培訓實力";
$lang["whychoose4_detail"]="在Intelli 才藝訓練中心學習的學生有90%以上成功申請進入著名的私立學校和大學";
$lang["home_contactus"]="聯絡我們";



/**********************************/
/*            About US            */
/**********************************/

$lang["aboutus1_title"] = " Intelli才藝訓練中心";
$lang["aboutus1_detail"] = "Intelli Training Center於2002年在加拿大溫哥華成立，隨著移民人數和外國留學生的增加， 我們的創辦人理解到要融入新環境難免會有不適應的狀況和相關的困難。我們的目標在於幫助多元文化家庭的孩子方提供指導，讓孩子不僅能夠成功地融入加拿大教育體系，還能夠成為一個快樂、自信並對知識有渴求的人。</p>
<p>Intelli 才藝訓練中心根據個別學生的學習水平制定漸進式培訓計劃，以達到不同的學習目標。使我們每一個學生都能夠培育和拓展自己獨特的興趣和成就。";

$lang["aboutus2_title"] = "評估和建意";
$lang["aboutus2_detail"] = "我們致力協助每一個孩子在學習上遇到的困難，並會提供即時建議或評估，務求加強學生的學習興趣以及鞏固基礎。同時亦會與家長之間保持緊密溝通，讓家長同步了解孩子的學習進度。";

$lang["aboutus_need_help"] = "需要幫忙？ 現在就打電話給我們";



/**********************************/
/*            Classes             */
/**********************************/


$lang["classes_n_programs"] = "各項課程";

$lang["classes_category"] = array("all" => "全部", "study_classes" => "課後功課輔導", "elite_classes" => "精英班", "special_programs" => "特別課程", "age_4_5" => "4-5 歲", "age_6_7" => "6-7 歲", "age_8_13" => "8-13 歲");




$lang["preschool_learning_program"] = array("title" => "學前預備班", "description" => "本課程適合3至5歲的兒童在興鬆及有趣的環境中學習和探索。運用故事，多媒體，遊戲和手工等不同的活動，啟發孩子對學習的熱愛，同時鼓勵他們勇於表達自己，並培養在群體活動中表現自信。我們的課程促進學生的閱讀和寫作能力，提高他們的詞彙量和英語會話的技巧，以及基本數學的知識。幫助孩子們實現學習的里程碑，為他們的學習能力奠定基礎在開始上學前作好準備。");

$lang["study_class"] = array("title" => "課後功課輔導", "description" => "適合目前在幼兒班到12年班學習的學齡兒童。此課程旨在幫助學生增強對校內課程的理解能力，以及指導學生完成學校作業。 我們會緊貼跟進每位學生的學習進度，并制定針對個別學術需求的學習計劃。通過成功的指導策略，我們會改進個別學生需要特別注意或增強的學習領域，鼓勵學生實現學習目標，並激發他們將學習的熱情帶回日常課程。");

$lang["intelli_social_network_club"] = array("title" => "Intelli 社交互動班", "description" => "孩子們良好的社交技巧實在可以推測將來的成功率。Intelli社交互動班是通過棋盤遊戲或其他團體活動，拓寬學生視野和擴展社交技能的最佳場所。孩子們將學習如何合作、貢獻、參與、遵循指示、設定共同目標及幫助他人；同時學習到談判、解決衝突、互相溝通，控制情緒、建立自信，爭取利益及公開演說等課堂以外的技能。孩子們可讓成無所畏懼地面對未來的挑戰或應對複雜情況的能力。");




$lang["singapore_math"] = array("title" => "新加坡數學", "description" => "新加坡數學方法是一種非常有效的教學方案，在國際數學比賽中往往名列前茅，在北美越來越多學校普及這教學方法。現金學生不僅需要以死記硬背的學習方法才能在數學上脫穎而出。通過應用新加坡數學，我們的學生們可專注學習各種學術技能，例如批判性思維，啟發式，邏輯推理，和建模技術等。 參加會議的學生將具備各種技能，可以自信地應對數字世界中的未來挑戰。");

$lang["100_series"] = array("title" => "100 系列", "description" => "100系列課程會通過許多有趣練習訓練孩子建立敏銳的思維。分為數學，英語，寫作，拼寫和閱讀五個類別，主要幫助孩子增強專注力並指導他們解決學習中遇到的困難。我們的100系列課程內容全面，涵蓋了如何解決問題，訓練批判性思維以及增強學校必需的基本知識，在幫助兒童建立學習基礎上提供的最佳選擇。");

$lang["oxford_reading_tree"] = array("title" => "牛津閱讀樹", "description" => "牛津閱讀樹是英國排名第一的閱讀系列叢書，數以百萬計的兒童不僅學會了甚至愛上了閱讀。 我們提供的牛津閱讀樹課程適合4-12歲兒童，透過此課程孩子們將學習到閱讀，拼音，寫作能力，混合發音及詞彙等等。每本書都有精美及生動有趣的插圖，故事情節來源於日常生活，讀來倍感親切，讓妳欲罷不能。透過此課程我們將啟發學生的閱讀潛能，激發他們練習和提升語言技能，並在學校獲得優異成績，打造孩子一輩子的閱讀力！");

$lang["jump_series"] = array("title" => "Jump 系列", "description" => "Jump系列提供Jump Math和Jump English。選用 “Jump”一詞的概念是跳過級別。學生在透過問答和挑戰應對課題的過程中啟發學術的理解能力。 此外，老師將提供積極的指導和評語，以幫助學生對這些概念有更深入的了解。 參加這些課程的學生往往會很快掌握並接受難道更高的挑戰，並在課堂上有“跳躍”的表現。");

$lang["summer_get_set_go"] = array("title" => "暑期 Get Set Go!", "description" => "我們的暑期課程專門為幫助孩子們在下個一年級前做好準備。課程結合英語和數學，內容精彩及充滿樂趣。 在整個暑假期間，學生將能夠保持敏銳的頭腦。通過不同的活動，他們會積極提高閱讀及理解能力，會話能力，擴大詞彙量並在獲得樂趣的同時，一步一步地邁向成功之道。在課程結束時，您的孩子將裝備好足夠的能力接受新學年的任何挑戰。");

$lang["chinese_classes"] = array("title" => "中文班", "description" => "中文是世界上使用最廣泛的語言之一。 每個文字都反映出古代的中國文化藝術。學習漢語作為第二語言將為孩子開拓未來。適合4歲及以上的兒童，我們的課程將重點介紹中文的讀寫技巧，聽力和對話以及道德教育等。孩子們亦會學習中文語法，諺語，故事，歷史和傳統節日。 此課程是以英語為母語的學生量身定制的，讓他們可以了解傳統中國文化價值和意義。");

$lang["reading_genius"] = array("title" => "閱讀天才", "description" => "孩子們應該在年幼時培養閱讀技巧，但要成為閱讀天才便需要加強培訓。 在Intelli才藝訓練中心，我們會提供能激發孩子們閱讀奇心的閱讀素材，使閱讀之旅引人入勝並身臨其境。該課程適合6歲及以上的兒童，致力提高他們的理解力，同時學會流暢地閱讀及寫作。 我們會挑選並制定不同形式的互動閱讀素材，有助於引導和鼓勵每個學生的積極參與。");

$lang["computer_explorer"] = array("title" => "電腦探索", "description" => "學習電腦的基礎知識，有助孩子們在現今眾多不同形式的科技領域中大展技能。 隨著數碼世界越來越普及，孩子們須具備探索這個新世界必要的知識和工具。 我們的年輕探險家將學習電腦的不同用途，包括文書處理，基本電腦概念，網站導航以及如何在網絡世界中獲取適當及安全的資訊等。 電腦知識已經從競爭優勢轉向必需技能的數碼化世界，在兒童教育中引入電腦概念永遠不會過早。");

$lang["canadian_101"] = array("title" => "加拿大知多D", "description" => "加拿大是一個多元文化的國家，是世界上最多種族的國家之一。您的孩子可以從課程中學習和探索加拿大與您來自的國家不同的地方。適合9歲及以上兒童，本課程透過閱讀相關文章，向孩子們介紹這個國家的重要特徵，如加拿大氣候，天然資源，地理分析，生活方式，加拿大文化和當前的加拿大活動等。 這有助於擴闊孩子們對加拿大的視野，同時提升他們閱讀和理解文章信息的能力。");

$lang["wow_in_the_world"] = array("title" => "奇趣世界", "description" => "適合11歲或以上的兒童，本課程旨在鼓勵學生緊貼身邊社區或世界發生的大小事。 孩子們可以體驗到閱讀小說以外，即時及相關的新聞文章，從而培養出分析能力和邏輯推理的技能。透過閱讀有關國際、加拿大或社區的報道，甚至是科學、體育、趣味或即時新聞的文章，孩子們都會感到深深著迷。 讓你的孩子們展開奇趣世界之旅吧！");

$lang["typing_guru"] = array("title" => "打字達人", "description" => "生活在現今的數碼世界，打字已成為兒童需要學習的重要技能。通過一系列由專業設計及簡易上手的互動打字練習程式，學生們會熟悉手指在鍵盤上擺放位置，同時訓練手的肌肉記憶把每一個按鍵位置不只不覺中牢牢記住了。學生可以學習如何有效流暢地打字，同時增速度，準確性和使用電腦的信心，更有助於詞彙及拼字和寫作技巧。");




$lang["creative_kids"] = array("title" => "創意塗鴉", "description" => "拿起蠟筆，顏色筆，膠水和閃粉，讓孩子的創造力閃耀吧！ 有趣及充滿創意的工作室，所有孩子都會喜歡！孩子可以探索不同的材料、顏色或圖案的搭配，以創作表達其創造力和個性的藝術作品。參與藝術或手工藝創作可以讓孩子們對生活更加熱情，也可以受益於情感表達上，並對提升學習能力，社交技巧或增強自信心有相當顯著的效果。");

$lang["stem"] = array("title" => "Stem", "description" => "在我們的21世紀，科技創新變得越來越重要，這改變了學生日常學習，聯繫和互動的模式。 STEM是基於現實世界的應用模式，融合科學、技術、工程和數學知識的綜合教學方法。我們會透過不同項目的學習過程，向孩子們介紹各種STEM職業，技能和工具，同時學習如何應用在現實世界中。 學生在學習過程中所開發的技能，可助他們轉化未來夢想為現實，並在學校及其他方面皆獲得成功。");

$lang["lego_program"] = array("title" => "樂高樂", "description" => "此樂高課程旨在激發孩子先天的好奇心，在制作模型的過程中充滿趣味及啟發性，可助發展溝通技巧、想像力、團隊合作精神以及邏輯思維的能力。樂高機械系列更讓學生深入了解結構、數學和物理的關係，探索STEM的核心概念。學生可以像年輕的科學家和工程師一樣工作，通過建造，設計和測試解決方案，同時領誤到現實世界的運作方式。");

$lang["junior_chef"] = array("title" => "小廚神", "description" => "一個充滿歡樂、趣味及美味的課堂，最適合你家中的小廚神。孩子們可以通過享受烹飪的樂趣學習質感、味道和風味，同時練習一些基本的數學概念及建立更多詞彙。這個美食課堂可以磨練我們的小廚神如何作掌握精細動作的技巧，學習如何攪拌，打泡，擀麵，灑潑，甚至用使用塑料刀切割。 從玩麵團或其他食材到製作糕點小吃，讓大家一同享受美味的成果。");

$lang["summer_speak_up_class"] = array("title" => "暑期演講班", "description" => "優質的公開演說技巧是一種無形的資產，有助孩子將來有機會成為偉大的領導者。我們的演講班可讓孩子們學到自己撰寫講詞，與觀眾互動，對聲線及語調的掌握，以及建立信心和發揮個人魅力。我們的目標是培養一個外向、有組織能力、積極進取的孩子。你會被你的孩子如何在舞台上閃耀為之驚訝，這無價的潛力可讓他們中生受益。");

$lang["summer_field_trip"] = array("title" => "暑期體驗班", "description" => "暑期體驗班不僅樂趣無窮，而且對孩子來說也是非常寶貴的學習經歷。 我們的課程可以讓孩子們感受與新環境互動的樂趣，同時提供了在教室以外探索和學習通識教學的機會。 無論是參觀農場，博物館還是在珍珠奶茶店擔任初級調茶師，不同的體驗都能讓學生與學校和“現實世界”之間建立聯繫。 通過體驗班進行實地考察，可讓孩子學習對不同的文化和整個社會更加了解和尊重。");





/**********************************/
/*            Contact US          */
/**********************************/


$lang["contactus_title"] = "保持聯繫";
$lang["contactus_intro"] = "請關注我們Facebook帳號緊貼最新消息，以及隨時歡迎向我們查詢更多課程資料。";
$lang["contactus_follow"] = "關注我們";
?>
